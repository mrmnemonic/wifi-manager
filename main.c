#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ROWS (256)
#define MAX_COLUMNS (256)
#define FILE_NAME "myInputFile"

char get_wifi_list[]="iwlist wlan0 scanning | egrep 'Quality|Encryption|ESSID' | sed -e 's/^[ \t]*//' | sed -e 's/Signal level.*//'";

static char words[MAX_ROWS][MAX_COLUMNS] = {{'\0','\0'}};

void BubbleSortWordsArray(int wordCount)
{
    int c, d;
    char swap[MAX_COLUMNS] = {'\0'};

    for(c=0;c<(wordCount-1);c++)
	{
		for(d=0;d<(wordCount-c-1);d++)
		{
			if(0>strcmp(words[d],words[d+1]))
			{
				strcpy(swap,words[d]);
				strcpy(words[d],words[d+1]);
				strcpy(words[d+1],swap);
            }
        }
    }
}

void PrintWordsArray(int wordCount)
{
    int i;

    printf("\n");
    for(i=0; i<wordCount; i++)
    {
        printf( "%s\n", words[i] );
    }
}

void RemoveSpaces(char* source)
{
	char* i = source;
	char* j = source;
	while(*j != 0)
	{
		*i = *j++;
		if(*i != ' ')
			i++;
	}
	*i = 0;
}

int main(int argc, char *argv[])
{
    FILE *fp = NULL;
    char wifi_signal[16];
    char wifi_encryption[8];
    char wifi_essid[256];

    if(NULL == (fp=popen(get_wifi_list, "r")))
    {
        printf("Error accessing Wi-Fi scan.\n");
		return 0;
    }

    // read each line from file into entry in words array
    int i=0, j=0;
    while(fgets(words[i], MAX_COLUMNS, fp ))
    {
        /* remove trailing newline from string
        words[i][strlen(words[i])-1] = '\0';
        i++;
        */
        words[i][strlen(words[i])-1] = '\0';
        RemoveSpaces(words[i]);
        printf("%s", words[i]);
        j++;
        if(j>2){
			j=0;
			putchar('\n');
		}
    }

    //BubbleSortWordsArray(i);
    //PrintWordsArray(i);

    return(0);
}
